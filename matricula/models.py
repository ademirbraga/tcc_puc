# -*- coding: utf-8 -*-
from django.db.models import CharField, BooleanField, ForeignKey
from util.models import Padrao
from cadastro.models import Cadastro
from curso.models import Curso
from disciplina.models import Disciplina

class Matricula(Padrao):
    class Meta:
        verbose_name = u'Matrícula'
        verbose_name_plural = u'Matrículas'
        db_table = 'matricula'

    numero   = CharField(verbose_name=u'Número', max_length=25, blank=False, null=False, unique=True)
    cadastro = ForeignKey(Cadastro, verbose_name=u'Cadastro', related_name='matricula_cadastro', null=False, blank=False)
    curso    = ForeignKey(Curso, verbose_name=u'Curso', related_name='matricula_curso', null=False, blank=False)
    ativo    = BooleanField(verbose_name=u'Ativo', default=True)

    def __unicode__(self):
        return u'%s - %s' % (self.numero, self.cadastro.nome)


class MatriculaDisciplina(Padrao):
    class Meta:
        verbose_name = u'Matrícula/Disciplina'
        verbose_name_plural = u'Matrícula/Disciplinas'
        db_table = 'matricula_disciplina'

    matricula  = ForeignKey(Matricula, verbose_name=u'Curriculo', related_name='matricula_matricula', null=False, blank=False)
    disciplina = ForeignKey(Disciplina, verbose_name=u'Disciplina', related_name='matricula_disciplina', null=False, blank=False)
    trancado   = BooleanField(verbose_name=u'Trancado?', default=False)

    def __unicode__(self):
        return u'%s - %s' % (self.matricula.numero, self.disciplina.codigo)