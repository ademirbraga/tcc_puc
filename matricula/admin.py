# -*- coding: utf-8 -*-
from models import MatriculaDisciplina, Matricula
from django.contrib.admin import site, TabularInline, ModelAdmin
from django.db import models
from django.forms import TextInput, Textarea


class MatriculaDisciplinaAdminInline(TabularInline):
    model = MatriculaDisciplina
    extra = 0

class MatriculaAdmin(ModelAdmin):
    list_display    = ['numero', 'cadastro', 'curso', 'ativo']
    list_filter     = ['numero', 'cadastro', 'curso', 'ativo']
    search_fields   = ['numero', 'cadastro', 'curso', 'ativo']
    ordering        = ['numero', 'cadastro', 'curso', 'ativo']

    save_as = True
    inlines = [
        MatriculaDisciplinaAdminInline,
    ]

    formfield_overrides = {
        models.CharField: {'widget': TextInput(attrs={'size': '50'})},
        models.TextField: {'widget': Textarea(attrs={'rows': 2, 'cols': 300})},
    }

site.register(Matricula, MatriculaAdmin)