# -*- coding: utf-8 -*-
from django.db.models import CharField, BooleanField, DateField, IntegerField
from util.models import Padrao

class Disciplina(Padrao):
    class Meta:
        verbose_name = u'Disciplina'
        verbose_name_plural = u'Disciplinas'
        db_table = 'disciplina'

    nome            = CharField(verbose_name=u'Nome', max_length=255, blank=False, null=False)
    codigo          = CharField(verbose_name=u'Código do Curso', max_length=14, blank=False, null=False, unique=True)
    creditos        = IntegerField(verbose_name=u'Créditos Totais', blank=False, null=False)
    data_criacao    = DateField(verbose_name=u'Data Criação', blank=True, null=True)
    ativo           = BooleanField(verbose_name=u'Ativo?', default=True)

    def __unicode__(self):
        return u'%s - %s' % (self.nome, self.codigo)