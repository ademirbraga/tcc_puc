# -*- coding: utf-8 -*-
from models import Disciplina
from django.contrib.admin import site, ModelAdmin
from django.db import models
from django.forms import TextInput, Textarea

class DisciplinaAdmin(ModelAdmin):
    list_display    = ['nome', 'codigo', 'creditos', 'ativo']
    list_filter     = ['nome', 'codigo', 'creditos', 'ativo']
    search_fields   = ['nome', 'codigo', 'creditos', 'ativo']
    ordering        = ['nome', 'codigo', 'creditos', 'ativo']

    save_as = True

    formfield_overrides = {
        models.CharField: {'widget': TextInput(attrs={'size': '50'})},
        models.TextField: {'widget': Textarea(attrs={'rows': 2, 'cols': 300})},
    }

site.register(Disciplina, DisciplinaAdmin)