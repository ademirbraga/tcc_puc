**DashBoard**
----------

**Dashboard** Painel administrativo desenvolvido em Python 2.7/MySQL utilizando o framework Django 1.11.2.

**Requisitos**
 - [Instalar Pip](https://www.rosehosting.com/blog/how-to-install-pip-on-ubuntu-16-04/ "PIP")
 
 

    sudo apt-get update && sudo apt-get -y upgrade
    sudo apt-get install python-pip

 
**Instalação**

Primeiramente é necessário fazer o download dos arquivos:

git clone https://gitlab.com/COOPERU/dashboard.git
Navegue até o diretório raiz /dashboard e execute o seguinte comando para instalar as dependências:

    pip install requirements.txt    ou
    pip install -r requirements.txt
    
**Configuração**
    
    python manage.py migrate  #para criar a nova base de dados
    python manage.py createsuperuser #para criar as configurações iniciais de acesso



**Iniciar**

Para iniciar a aplicação basta rodar o seguinte comando:

**python manage.py runserver**

Por padrão o serviço é inicializado na porta 8000.

![enter image description here](https://lh3.googleusercontent.com/-z1SHnFpFbnU/WYkl7ynGjhI/AAAAAAAAHTE/to2WQ5gBA1gztPQHplqMGBTg2SGYI2W8ACLcBGAs/s0/dashboard1.png "dashboard1.png")


**FrontEnd**

![enter image description here](https://lh3.googleusercontent.com/-smpZ_Xo9cR4/WYkmeHxV1hI/AAAAAAAAHTQ/t6UEZcZ9jrgMREDAdakzRaKFGopJGu6DQCLcBGAs/s0/dashboard2.png "dashboard2.png")
