# -*- coding: utf-8 -*-
from django.db.models import CharField, BooleanField, ForeignKey, DateField, IntegerField
from util.models import Padrao
from cadastro.models import Cadastro

class TipoCurso(Padrao):
    class Meta:
        verbose_name = u'Tipo Curso'
        verbose_name_plural = u'Tipos Cursos'
        db_table = 'tipo_curso'

    nome = CharField(verbose_name=u'Nome', max_length=255, blank=False, null=False)

    def __unicode__(self):
        return u'%s' % (self.nome)


class Curso(Padrao):
    class Meta:
        verbose_name = u'Curso'
        verbose_name_plural = u'Cursos'
        db_table = 'curso'

    nome            = CharField(verbose_name=u'Nome', max_length=255, blank=False, null=False)
    tipo_curso      = ForeignKey(TipoCurso, verbose_name=u'Tipo curso', related_name='curso_tipo', null=False, blank=False)
    codigo          = CharField(verbose_name=u'Código do Curso', max_length=14, blank=False, null=False, unique=True)
    data_criacao    = DateField(verbose_name=u'Data Criação', blank=True, null=True)
    ativo           = BooleanField(verbose_name=u'Ativo?', default=True)

    coordernador    = ForeignKey(Cadastro, verbose_name=u'Coordernador', related_name='curso_coordenador', null=True, blank=True)

    def __unicode__(self):
        return u'%s - %s' % (self.nome, self.codigo)
