# -*- coding: utf-8 -*-
from models import Curso, TipoCurso
from django.contrib.admin import site, TabularInline, ModelAdmin
from django.db import models
from django.forms import TextInput, Textarea

class TipoCursoAdmin(ModelAdmin):
    model = TipoCurso
    extra = 0
    sortable_field_name = 'nome'

class CursoAdmin(ModelAdmin):
    list_display    = ['nome', 'tipo_curso', 'codigo']
    list_filter     = ['nome', 'tipo_curso', 'codigo']
    search_fields   = ['nome', 'tipo_curso', 'codigo']
    ordering        = ['nome', 'tipo_curso', 'codigo']

    save_as = True

    formfield_overrides = {
        models.CharField: {'widget': TextInput(attrs={'size': '50'})},
        models.TextField: {'widget': Textarea(attrs={'rows': 2, 'cols': 300})},
    }

site.register(TipoCurso, TipoCursoAdmin)
site.register(Curso, CursoAdmin)