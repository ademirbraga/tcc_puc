# -*- coding: utf-8 -*-
from models import Curriculo, DisciplinaCurriculo
from django.contrib.admin import site, TabularInline, ModelAdmin
from django.db import models
from django.forms import TextInput, Textarea


class DisciplinaAdminInline(TabularInline):
    model = DisciplinaCurriculo
    extra = 0

class CurriculoAdmin(ModelAdmin):
    list_display    = ['nome', 'codigo', 'curso', 'ativo']
    list_filter     = ['nome', 'codigo', 'curso', 'ativo']
    search_fields   = ['nome', 'codigo', 'curso', 'ativo']
    ordering        = ['nome', 'codigo', 'curso', 'ativo']

    save_as = True
    inlines = [
        DisciplinaAdminInline,
    ]

    formfield_overrides = {
        models.CharField: {'widget': TextInput(attrs={'size': '50'})},
        models.TextField: {'widget': Textarea(attrs={'rows': 2, 'cols': 300})},
    }

site.register(Curriculo, CurriculoAdmin)