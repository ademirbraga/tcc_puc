# -*- coding: utf-8 -*-
from django.db.models import CharField, BooleanField, ForeignKey, DateField, IntegerField, ManyToManyField
from util.models import Padrao
from curso.models import Curso
from disciplina.models import Disciplina

class Curriculo(Padrao):
    class Meta:
        verbose_name = u'Curriculo'
        verbose_name_plural = u'Curriculos'
        db_table = 'curriculo'
        ordering = ('nome',)

    nome            = CharField(verbose_name=u'Nome', max_length=255, blank=False, null=False)
    codigo          = CharField(verbose_name=u'Código do Currículo', max_length=14, blank=False, null=False, unique=True)
    data_criacao    = DateField(verbose_name=u'Data Criação', blank=True, null=True)
    ativo           = BooleanField(verbose_name=u'Ativo?', default=True)
    curso           = ForeignKey(Curso, verbose_name=u'Curso', related_name='curriculo_curso', null=False, blank=False)

    def __unicode__(self):
        return u'%s - %s' % (self.nome, self.codigo)


class DisciplinaCurriculo(Padrao):
    class Meta:
        verbose_name = u'Disciplina/Curriculo'
        verbose_name_plural = u'Disciplinas/Currículos'
        db_table = 'disciplina_curriculo'

    curriculo  = ForeignKey(Curriculo, verbose_name=u'Curriculo', related_name='disciplina_curriculo', null=True, blank=True)
    disciplina = ForeignKey(Disciplina, verbose_name=u'Disciplina', related_name='disciplina_disciplina', null=True, blank=True)
    ativo      = BooleanField(verbose_name=u'Ativo?', default=True)

    def __unicode__(self):
        return u'%s - %s' % (self.curriculo.codigo, self.disciplina.codigo)