# -*- coding: utf-8 -*-
from models import Cadastro, Telefone, Vinculo
from django.contrib.admin import site, TabularInline, ModelAdmin
from django.db import models
from django.forms import TextInput, Textarea


class VinculoAdminInline(ModelAdmin):
    model = Vinculo
    extra = 0
    sortable_field_name = 'nome'

class TelefoneAdminInline(TabularInline):
    model = Telefone
    extra = 0
    sortable_field_name = 'numero'

class CadastroAdmin(ModelAdmin):
    list_display    = ['nome', 'email', 'cpf_cnpj', 'vinculo']
    list_filter     = ['nome', 'email', 'cpf_cnpj', 'vinculo']
    search_fields   = ['nome', 'email', 'cpf_cnpj', 'vinculo']
    ordering        = ['nome', 'email', 'cpf_cnpj', 'vinculo']

    save_as = True
    inlines = [
        TelefoneAdminInline,
    ]

    formfield_overrides = {
        models.CharField: {'widget': TextInput(attrs={'size': '50'})},
        models.TextField: {'widget': Textarea(attrs={'rows': 2, 'cols': 300})},
    }


site.register(Vinculo, VinculoAdminInline)
site.register(Cadastro, CadastroAdmin)