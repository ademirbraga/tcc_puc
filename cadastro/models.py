# -*- coding: utf-8 -*-
from django.db.models import CharField, EmailField, ForeignKey, DateField, IntegerField

from settings import ESTADOS_BRASILEIROS, SEXO, TIPO_TELEFONE
from util.models import Padrao


class Vinculo(Padrao):
    class Meta:
        verbose_name = u'Vínculo'
        verbose_name_plural = u'Vínculos'
        db_table = 'vinculo'

    nome = CharField(verbose_name=u'Nome', max_length=255, blank=False, null=False)

    def __unicode__(self):
        return u'%s' % (self.nome)


class Cadastro(Padrao):
    class Meta:
        verbose_name = u'Cadastro'
        verbose_name_plural = u'Cadastros'
        db_table = 'cadastro'

    nome            = CharField(verbose_name=u'Nome', max_length=255, blank=False, null=False)
    email           = EmailField(verbose_name=u'E-mail', max_length=255, blank=False, null=False, unique=True)
    senha           = CharField(verbose_name=u'Senha', max_length=150, blank=False, null=False)
    cpf_cnpj        = CharField(verbose_name=u'CPF/CNPJ', max_length=14, blank=True, null=True, unique=True)
    vinculo         = ForeignKey(Vinculo, verbose_name=u'Vínculo', related_name='vinculo_pessoa', null=False, blank=False)
    data_nascimento = DateField(verbose_name=u'Data Nascimento', blank=True, null=True)
    sexo            = CharField(verbose_name=u'Sexo', choices=SEXO, max_length=1)

    logradouro = CharField(verbose_name=u'Logradouro', max_length=150)
    numero = CharField(verbose_name=u'Número', max_length=10)
    cep = CharField(verbose_name=u'CEP', max_length=10, blank=True)
    bairro = CharField(verbose_name=u'Bairro', max_length=150)
    complemento = CharField(verbose_name=u'Complemento', max_length=150, blank=True)
    cidade = CharField(verbose_name=u'Cidade', max_length=150)
    estado = CharField(verbose_name=u'Estado', choices=ESTADOS_BRASILEIROS, max_length=2)

    def __unicode__(self):
        return u'%s - %s' % (self.nome, self.cpf_cnpj)



class Telefone(Padrao):
    class Meta:
        verbose_name = u'Telefone'
        verbose_name_plural = u'Telefones'
        db_table = 'telefone'

    cadastro = ForeignKey(Cadastro, verbose_name=u'Cadastro', related_name='cadastro_telefone', null=True, blank=True)
    tipo     = IntegerField(verbose_name=u'Tipo Telefone', choices=TIPO_TELEFONE, blank=False, null=False)
    numero   = CharField(verbose_name=u'Número', max_length=20, blank=False, null=False)

    def __unicode__(self):
        return u'%s - %s' % (self.tipo, self.numero)